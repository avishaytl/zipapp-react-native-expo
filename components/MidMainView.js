import React from 'react';
import { View, StyleSheet, Text, TouchableOpacity, Modal, TextInput,ImageBackground} from 'react-native'; 
import primaryTheme from '../styles/styles';  
import * as Icons from '@expo/vector-icons';
export default class MidMainView extends React.Component{
  constructor(props){
    super(props);
    this.state = { 
        text: `הנחות עד`,
        date: '20/03/2020',
        centerText: '50%'
    }
  }
    render(){  
          return(
            <View style={styles.container}>  
                <View style={[styles.triangleCorner,{left:-1}]}/>
                <View style={[styles.triangleCorner,styles.triangleRight,{right:-1}]}/>
                <View style={{height:10,position:'absolute',right:0,top:10,width:'100%',backgroundColor:'white'}}></View>
                <View style={{height:10,position:'absolute',left:0,bottom:10,width:'100%',backgroundColor:'white'}}></View>
                <Text style={{color:'white',fontSize:14,fontWeight:'bold',position:'absolute',right:0,top:20,width:'100%',paddingRight:15}}>{this.state.text}</Text>
                <Text style={{color:'white',fontSize:14,fontWeight:'bold',position:'absolute',left:0,bottom:22,width:'100%',paddingLeft:15}}>{this.state.date}</Text>
                <Text style={{color:'white',fontSize:150,fontWeight:'bold'}}>{this.state.centerText}</Text>
            </View>
          ); 
    }
  }
  const styles = StyleSheet.create({
    container: {   
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent:'center',
        width:'90%', 
        height: '90%',     
        backgroundColor:'black', 
    },    
    triangleCorner: {
        position:'absolute',
        width: 0,
        height: '100%',   
        backgroundColor: 'transparent',
        borderStyle: 'solid',
        borderRightWidth: 40,
        borderTopWidth: 240,
        borderRightColor: 'transparent',
        borderTopColor: 'white'
      },
      triangleRight: {
        transform: [
          {rotate: '180deg'}
        ]
      },
  });
     