import React from 'react';
import { View, StyleSheet, Text, TouchableOpacity, Modal, TextInput,ImageBackground} from 'react-native'; 
import primaryTheme from '../styles/styles';  
import * as Icons from '@expo/vector-icons';
export default class HeaderApp extends React.Component{
  constructor(props){
    super(props);
    this.state = { 
    }
  }
    render(){  
          return(
            <View style={styles.container}>
                <View style={{flex:1,height:50,justifyContent:'flex-start',alignItems:'center',flexDirection:'row'}}> 
                    <ImageBackground  resizeMode={'stretch'} source={primaryTheme.$logo}  style={styles.logo}/>
                    <TouchableOpacity style={{justifyContent:'flex-start',alignItems:'center',flexDirection:'row'}} onPress={()=>console.log('category')}> 
                        <Icons.MaterialCommunityIcons style={[styles.btnIcon]} name={'menu-down-outline'}/> 
                        <Text>קטגוריות</Text>
                    </TouchableOpacity>
                </View> 
                <View style={{flex:1,height:50,justifyContent:'flex-end',alignItems:'center',flexDirection:'row'}}> 
                    <TouchableOpacity onPress={()=>console.log('like1')}>
                        <View style={styles.btnCircle}>
                            <Icons.AntDesign style={[styles.btnIcon]} name={'like1'}/>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={()=> this.props.navigate('search')}>
                        <View style={styles.btnCircle}>
                            <Icons.FontAwesome style={[styles.btnIcon]} name={'search'}/>
                        </View>
                    </TouchableOpacity>
                </View>
            </View>
          ); 
    }
  }
  const styles = StyleSheet.create({
    container: {  
        position:'absolute',
        top: primaryTheme.$deviceStatusBar,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent:'center',
        width: primaryTheme.$deviceWidth,
        height: 50,  
        borderBottomColor:'#b4b4b4',
        borderBottomWidth:0.5, 
    }, 
    logo:{ 
        width:50,
        height:23,
        margin:5,
    },
    btnCircle:{
        width:30,
        height:30,
        borderRadius:30/2,
        margin:5,
        backgroundColor:'white',
        alignItems:'center',
        justifyContent:'center'
    },
    btnIcon:{
        fontSize:20
    }
  });
     