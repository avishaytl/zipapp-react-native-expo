import React from 'react';
import { View, StyleSheet, Text, TouchableOpacity, Modal, TextInput} from 'react-native'; 
export default class ModalApp extends React.Component{
  constructor(props){
    super(props);
    this.state = {
      isVisible: this.props.isVisible, 
    }
  }
    render(){ 
        let data = this.props.data;
          return(
            <Modal animationType="fade" transparent={true} visible={this.state.isVisible}>
                <View style={styles.modalView}>
                    <TouchableOpacity activeOpacity={1} style={{width:'100%',height:'100%',zIndex:1000}} onPress={() => {
                        if(this.state.isVisible){
                            this.setState({isVisible: false});
                            this.props.changeModalView(false);
                        }else
                            this.setState({isVisible: true});
                        }}/>
                    <View style={styles.modalViewInside}> 
                       <Text>{this.props.data.mobile}</Text>
                    </View>
                </View>
            </Modal>
          ); 
    }
  }
  const styles = StyleSheet.create({
    modalView:{
      width:'100%',
      height:'100%',
      flexDirection:'column',
      alignItems:'center',
      justifyContent:'center',
      backgroundColor:'rgba(0,0,0,0)',
    },
    modalViewInside:{
      position:'absolute',
      width:'80%',
      height:300,
      flexDirection:'column',
      alignItems:'center',
      justifyContent:'center',
      zIndex:2000,  
      backgroundColor:'white',
      borderRadius: 10,
      borderColor: 'rgba(0,0,0,0.25)',
      borderWidth: 1
    }, 
  });
     