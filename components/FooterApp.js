import React from 'react';
import { View, StyleSheet, Text, TouchableOpacity, Modal, TextInput} from 'react-native'; 
import primaryTheme from '../styles/styles'; 
import { AntDesign, FontAwesome, Ionicons, Entypo } from '@expo/vector-icons';
export default class FooterApp extends React.Component{
  constructor(props){
    super(props);
    this.state = { 
        data:{
            btn1Title: 'Main',
            btn2Title: 'Trades',
            btn3Title: 'News',
            btn4Title: 'Settings',
        }
    }
  }
    render(){  
          return(
            <View style={styles.container}>
                <TouchableOpacity onPress={()=>console.log(this.state.data.btn1Title)} style={styles.btn}>
                    <Entypo style={[styles.btnIcon,{paddingRight:4}]} name={'shareable'}/>
                    <Text style={styles.btnStyle}>{this.state.data.btn1Title}</Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={()=>console.log(this.state.data.btn2Title)} style={styles.btn}>
                    <Ionicons style={[styles.btnIcon,{paddingRight:4}]} name={'md-stats'}/>
                    <Text style={styles.btnStyle}>{this.state.data.btn2Title}</Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={()=>console.log(this.state.data.btn3Title)} style={styles.btn}>
                    <AntDesign style={[styles.btnIcon,{paddingRight:4}]} name={'CodeSandbox'}/>
                    <Text style={styles.btnStyle}>{this.state.data.btn3Title}</Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={()=>console.log(this.state.data.btn4Title)} style={styles.btn}>
                    <FontAwesome style={[styles.btnIcon,{paddingRight:4}]} name={'cogs'}/>
                    <Text style={styles.btnStyle}>{this.state.data.btn4Title}</Text>
                </TouchableOpacity>
            </View>
          ); 
    }
  }
  const styles = StyleSheet.create({
    container: {  
        position:'absolute',
        bottom: 0,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent:'center',
        width: primaryTheme.$deviceWidth,
        height: 50, 
        backgroundColor:'black',
    }, 
    btn:{
        zIndex:22222,
        flex:1,
        alignItems:'center',
        flexDirection:'row',
        justifyContent:'center',
        height:50
    },
    btnStyle:{
        color: 'white',
        fontSize: 16
    },
    btnIcon:{
        color: 'white',
        fontSize: 20
    }
  });
     