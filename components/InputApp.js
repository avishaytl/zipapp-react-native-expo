import React from 'react';
import {  View, StyleSheet, Animated, Text, TouchableOpacity, TextInput} from 'react-native'; 
import primaryTheme from '../styles/styles'; 
import CountryPicker, { DARK_THEME, getAllCountries } from 'react-native-country-picker-modal';
import { LinearGradient } from 'expo-linear-gradient';
export default class InputApp extends React.Component{
  constructor(props){
    super(props);
    this.state = {  
        shift: new Animated.Value(0), 
        countryCodeSelected: 'IL', 
        countryNameSelected: 'israel',
        countryCallingcode: '+972',

    } 
  }  
  setBtnStyle = () =>{ 
    return this.props.theme === 'active' ? {
        placeHolderColor: 'black',
        inpBackcolors: 'white',
        borderColor: 'rgba(0,0,0,0)',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.20,
        shadowRadius: 5, 
        elevation: 20,
    } : { 
        placeHolderColor: 'black',
        inpBackcolors: 'rgba(0,0,0,0)',
        borderColor: 'white',
        shadowOpacity: 0,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 0,
        }, 
        shadowRadius: 0, 
        elevation: 0,
    };
  }
    render(){ 
        let style = this.setBtnStyle();
        if(this.props.placeholder !== 'Mobile')
          return(
            <LinearGradient start={[0.1, 0.4]} end={[0.9, 0.1]} colors={['#fff','#fff','#fff']} style={[styles.container,{borderWidth: 1, borderColor: '#fff'}]}>
                  <LinearGradient start={[0.1, 0.4]} end={[0.9, 0.1]} colors={['#fff','#fff','#fff']} style={[styles.container,{borderRadius: 56/2,width: primaryTheme.$deviceWidth - 76,height:54,borderColor:'rgba(0,0,0,0)'}]}>
                      
              {/* <View style={styles.container}> */}
                  <View style={{position:'absolute',right:20}}> 
                  {this.props.icon}
                  </View>
                  <TextInput onFocus={this.props.onPress}  style={[styles.container,{
                    shadowColor: style.shadowColor, 
                    shadowOffset: {width: style.shadowOffset.width,height: style.shadowOffset.height},
                    shadowOpacity: style.shadowOpacity,
                    shadowRadius: style.shadowRadius,
                    elevation: style.elevation,
                    fontSize:20,
                    paddingRight: 48,
                    backgroundColor: style.inpBackcolors,
                    borderWidth: 1, 
                    borderColor: style.borderColor,
                    color: style.placeHolderColor,
                    fontWeight:'bold'}]} placeholderTextColor={style.placeHolderColor} placeholder={this.props.placeholder} onEndEditing={() => this.props.onChangeText(this.state.text, this.state.placeholder)} onChangeText={text => this.setState({text: text, placeholder: this.props.placeholder})} value={this.props.value} />
              {/* </View> */}
                  </LinearGradient> 
                </LinearGradient> 
          );
        else
          return(
            <View style={[styles.container,{
              shadowColor: style.shadowColor, 
              shadowOffset: {width: style.shadowOffset.width,height: style.shadowOffset.height},
              shadowOpacity: style.shadowOpacity,
              shadowRadius: style.shadowRadius,
              elevation: style.elevation,
              borderColor: style.borderColor,
              color: style.placeHolderColor,
              backgroundColor: style.inpBackcolors,
            }]}>
                <View style={[styles.searchIcon]}> 
                    <CountryPicker 
                        countryList={getAllCountries().currency}
                        countryCode={this.state.countryCodeSelected}
                        withCallingCode={true}
                        withAlphaFilter={true} 
                        withFilter={true}  
                        withFlag={true}
                        onSelect={ value => this.setState( () => ({
                        countryCodeSelected: value.cca2,
                        countryNameSelected: value.name,
                        countryCallingcode: '+' + value.callingCode,
                        }))}/> 
                      <Text style={{fontSize:20,marginRight:5,fontWeight:'bold'}}>{this.state.countryCallingcode}</Text> 
                      <TextInput onFocus={this.props.onPress}  
                        keyboardType={'phone-pad'}
                        style={[{width: primaryTheme.$deviceWidth - 185,
                        fontSize:20, 
                        borderRadius: 60/2,
                        backgroundColor: style.inpBackcolors,
                        borderWidth: 1, 
                        borderColor: style.borderColor,
                        color: style.placeHolderColor,
                        fontWeight:'bold'}]} placeholderTextColor={style.placeHolderColor} placeholder={this.props.placeholder} onEndEditing={() => this.props.onChangeText(this.state.countryCallingcode + this.state.text, this.state.placeholder)} onChangeText={text => this.setState({text: text, placeholder: this.props.placeholder})} value={this.props.value} />
                </View>
               </View>
        );
    } 
}


  const styles = StyleSheet.create({
    container: {  
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent:'center',
        width: primaryTheme.$deviceWidth - 70,
        height: 60,
        margin:5,
        borderRadius: 60/2,
    },   
    searchIcon:{ 
      zIndex:5, 
      display: 'flex',
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'center',
      width: primaryTheme.$deviceWidth - 70,
      paddingLeft:10,
      height: 60, 
      borderRadius: 60/2,  
      paddingLeft: 20,
    }, 
  }); 