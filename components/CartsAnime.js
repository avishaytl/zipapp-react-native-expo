import React from 'react';
import { View, StyleSheet, Text, Animated, Modal, TextInput,ImageBackground} from 'react-native';  
import * as Icons from '@expo/vector-icons';
export default class CartsAnime extends React.Component{
  constructor(props){
    super(props);
    this.state = {  
        cartsAnime:{ 
          cart1: new Animated.Value(-10), 
          cart2: new Animated.Value(-250), 
        }
    } 
  }
  UNSAFE_componentWillMount(){ 
    this.anime();
  }  
  anime = () =>{  
    Animated.timing(
        this.state.cartsAnime.cart1,
        {toValue: 230,duration: 40000}
      ).start(()=>{ 
        this.state.cartsAnime.cart1.setValue(-10);
        Animated.timing(
            this.state.cartsAnime.cart1,
            {toValue: 230,duration: 40000}
          ).start();
      }); 
      Animated.timing(
          this.state.cartsAnime.cart2,
          {toValue: -10,duration: 40000}
        ).start(()=>{ 
        this.state.cartsAnime.cart2.setValue(-250);
        Animated.timing(
            this.state.cartsAnime.cart2,
            {toValue: -10,duration: 40000}
            ).start();
        }); 
  }
    render(){  
          return(
            <View style={styles.container}> 
                <Animated.View style={{position:'absolute',bottom:0,left:this.state.cartsAnime.cart1,flexDirection:'row'}}> 
                    <Icons.MaterialIcons style={{color: 'black',fontSize:30}} name={'shopping-cart'}/>
                    <Icons.MaterialIcons style={{color: 'black',fontSize:30}} name={'shopping-cart'}/>
                    <Icons.MaterialIcons style={{color: 'black',fontSize:30}} name={'shopping-cart'}/>
                    <Icons.MaterialIcons style={{color: 'black',fontSize:30}} name={'shopping-cart'}/>
                    <Icons.MaterialIcons style={{color: 'black',fontSize:30}} name={'shopping-cart'}/>
                    <Icons.MaterialIcons style={{color: 'black',fontSize:30}} name={'shopping-cart'}/>
                    <Icons.MaterialIcons style={{color: 'black',fontSize:30}} name={'shopping-cart'}/>
                    <Icons.MaterialIcons style={{color: 'black',fontSize:30}} name={'shopping-cart'}/>
                </Animated.View>
                <Animated.View style={{position:'absolute',bottom:0,left:this.state.cartsAnime.cart2,flexDirection:'row'}}> 
                    <Icons.MaterialIcons style={{color: 'black',fontSize:30}} name={'shopping-cart'}/>
                    <Icons.MaterialIcons style={{color: 'black',fontSize:30}} name={'shopping-cart'}/>
                    <Icons.MaterialIcons style={{color: 'black',fontSize:30}} name={'shopping-cart'}/>
                    <Icons.MaterialIcons style={{color: 'black',fontSize:30}} name={'shopping-cart'}/>
                    <Icons.MaterialIcons style={{color: 'black',fontSize:30}} name={'shopping-cart'}/>
                    <Icons.MaterialIcons style={{color: 'black',fontSize:30}} name={'shopping-cart'}/>
                    <Icons.MaterialIcons style={{color: 'black',fontSize:30}} name={'shopping-cart'}/>
                    <Icons.MaterialIcons style={{color: 'black',fontSize:30}} name={'shopping-cart'}/>
                </Animated.View>
            </View>
          ); 
    }
  }
  const styles = StyleSheet.create({
    container: {   
        width:208,
        height:100,   
        borderRadius:20, 
        alignItems:'center',
        justifyContent:'center',
        overflow:'hidden',
    },  
  });
     