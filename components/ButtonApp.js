import React from 'react';
import {  View, StyleSheet, Animated, Text, TouchableOpacity, Image} from 'react-native'; 
import primaryTheme from '../styles/styles';  
import { LinearGradient } from 'expo-linear-gradient';
export default class ButtonApp extends React.Component{
  constructor(props){
    super(props);
    this.state = {
        btnAnime:  new Animated.Value(1),   
    } 
  } 
  btnAnime = () =>{
    Animated.timing(
      this.state.btnAnime,
      {toValue: 0.95, duration: 100}
    ).start(()=>Animated.timing(
      this.state.btnAnime,
      {toValue: 1, duration: 100}
    ).start())
  } 
  setBtnStyle = () =>{ 
    return this.props.theme === 'active' ? {
        btnTitleColor: 'black',
        btnBackcolors: ['#6f6f6f','#363636'],
        borderColor: 'rgba(0,0,0,0)',
    } : { 
        btnTitleColor: 'black',
        btnBackcolors: ['#fff','#fff','#fff'],
        borderColor: '#fff',
    };
  }
    render(){ 
      let style = this.setBtnStyle();
      return(
          <TouchableOpacity activeOpacity={0.9} onPressIn={this.btnAnime} onPress={this.props.onPress}> 
            <Animated.View style={{transform: [{ scale: this.state.btnAnime }]}}>
                <LinearGradient start={[0.1, 0.4]} end={[0.9, 0.1]} colors={['#fff','#fff','#fff']} style={[styles.container,{borderWidth: 1, borderColor: style.borderColor}]}>
                  <LinearGradient start={[0.1, 0.4]} end={[0.9, 0.1]} colors={style.btnBackcolors} style={[styles.container,{borderRadius: 56/2,width: primaryTheme.$deviceWidth - 76,height:54,borderColor:'rgba(0,0,0,0)'}]}>
                      {this.props.icon}
                      <Text style={[styles.btnText,{color: style.btnTitleColor}]}>{this.props.name}</Text>
                  </LinearGradient> 
                </LinearGradient> 
            </Animated.View>
          </TouchableOpacity>
      );
    } 
}

  

  const styles = StyleSheet.create({
    container: {  
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent:'center',
      width: primaryTheme.$deviceWidth - 70,
      height: 60,
      margin:5,
      borderRadius: 60/2,
      shadowColor: "#000",
      shadowOffset: {
        width: 0,
        height: 2,
      },
      shadowOpacity: 0.25,
      shadowRadius: 1.84,

      elevation: 3,
    },  
    btnText:{ 
        fontSize:20,
        fontWeight:'bold'
    }
  }); 