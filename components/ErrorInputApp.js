import React from 'react';
import { View, StyleSheet, Text, TouchableOpacity, Modal, TextInput} from 'react-native'; 
import primaryTheme from '../styles/styles'; 
export default class ErrorInputApp extends React.Component{
  constructor(props){
    super(props);
    this.state = { 
    }
  }
    render(){  
          return(
            <View style={styles.container}>
                <Text style={styles.errorText}>{this.props.value}</Text>
            </View>
          ); 
    }
  }
  const styles = StyleSheet.create({
    container: {  
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent:'center',
        width: primaryTheme.$deviceWidth - 70,
        height: 34,
        margin:5,
        borderRadius: 60/2,
        backgroundColor:'black',
    },   
    errorText:{
        fontSize: 15,
        color: 'white',
    }
  });
     