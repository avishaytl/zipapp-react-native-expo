import primaryValues from '../constants/values';
import config from '../constants/config';
import { call, put } from 'redux-saga/effects';

const postRequest = (url, body,  headers = {'Content-type':'application/x-www-form-urlencoded; charset=UTF-8'}) =>
    fetch(url, {
        method: 'post',
        headers,
        body,
    })
        .then(response => response.json())
        .then(responseJson => responseJson)
        .catch(error => error);

//****************************//
//*********  LOGIN  **********//
//****************************//
export function* fetchLogin( action ) {
    action = action.params; 
    try {
        const url = config.url_main_api;
        const body = buildPostRequest(action , 0);
        const response = yield call(postRequest, url, body);
            if (response.success) {
                yield put({ type: primaryValues.$LOGIN_RESULT, response }); 
            } else 
                yield put({ type: primaryValues.$LOGIN_ERROR, response });
        } catch (e) { 
            console.debug('Login msg error - ' + e.message);
        }
};

//****************************//
//*********  HELPS  **********//
//****************************//
const buildPostRequest = (data, index) => { 
    let body = `&email=${data['email']}&password=${data['password']}&device_token=${''}&device_type=expo&company_id=${config.company_id}`;
    if(index == 0) return body;
    let bodyRequest = "";
    let count = 0;
    for (let key in data) {
        if (count++ > 0)
            bodyRequest += "&";
        bodyRequest += key + "=" + data[key];
    }
    return bodyRequest;
};