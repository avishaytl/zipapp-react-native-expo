import { Dimensions } from 'react-native';
import { getStatusBarHeight } from 'react-native-status-bar-height';// ** device width&height **
import { Platform } from 'react-native';

const statusbar = getStatusBarHeight();
const width = Dimensions.get('window').width;
const height = Platform.OS !== 'ios' ? Dimensions.get('window').height + statusbar : Dimensions.get('window').height;
console.debug('is iphone - ' + Platform.OS);
console.log('status bar - ' + statusbar);
console.log('is Height - ' + height);
console.debug('is width - ' + width);

const primaryTheme = {
    $deviceWidth: width,
    $deviceHeight: height,
    $deviceStatusBar: statusbar, 
    $loginBackground: require('./images/background.webp'), 
    $logo: require('./images/logo.webp'), 
};

export default primaryTheme;
