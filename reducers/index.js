import { combineReducers } from 'redux';
import reducer from './reducers';
const rootReducer = combineReducers({
  initialState: reducer,
}); 

export default rootReducer;