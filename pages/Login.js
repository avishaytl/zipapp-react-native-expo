import React from 'react';
import { Animated, Keyboard, UIManager, Dimensions, View, StyleSheet, TextInput, Text, ImageBackground, Image} from 'react-native';
import { connect } from 'react-redux'; 
import primaryTheme from '../styles/styles';
import ButtonApp from '../components/ButtonApp';
import InputApp from '../components/InputApp';  
import ModalApp from '../components/ModalApp'; 
import Counter from '../components/Counter'; 
import CartsAnime from '../components/CartsAnime'; 
import { TouchableOpacity } from 'react-native-gesture-handler';
import FadeIn from 'react-native-fade-in-image';
import { LinearGradient } from 'expo-linear-gradient';
import Flag from 'react-native-flags';
import * as Icons from '@expo/vector-icons'; 
const { State: TextInputState } = TextInput;  
class Login extends React.Component{
  constructor(props){
    super(props);
    this.state = {  
      shift: new Animated.Value(0), 
      textOpacity: new Animated.Value(1),
      loginAnime: new Animated.Value(0),
      signinAnime: new Animated.Value(-primaryTheme.$deviceWidth),
      signupAnime: new Animated.Value(-primaryTheme.$deviceWidth),
      logoAnime: new Animated.Value(0), 
      inputPress: null,
      isModalVisible: false,
      data:{
        mainText: 'אפליקציה 100% ישראלית למסחר, ע"י לחיצה על',
        btnTopTitle: 'הרשמה',
        btnBottomTitle: 'התחברות',
        inputTopTitle: 'אימייל',
        inputBottomTitle: 'סיסמא',
        linkTextForgot: 'שכחתי סיסמא',
        linkTextBack: 'חזור',
      },
    } 
  }  
  UNSAFE_componentWillMount() {
    this.keyboardDidShowSub = Keyboard.addListener('keyboardDidShow', this.handleKeyboardDidShow);
    this.keyboardDidHideSub = Keyboard.addListener('keyboardDidHide', this.handleKeyboardDidHide); 
  }

  componentWillUnmount() {
    this.keyboardDidShowSub.remove();
    this.keyboardDidHideSub.remove();
  }
  btnClick = (btnTitle, navigate) => {
    btnTitle === this.state.data.btnBottomTitle ? this.signinAnime('start') : this.signupAnime('start');
  }
  changeModalView = (val) =>{ 
    this.setState({isModalVisible: val});
  }
  onChangeTextInput = (val, inputName) => {
    console.log(val,inputName);
    console.log(this.state.inputPress);
  }
  signinAnime = (pos) =>{
    if(pos === 'start'){
      Animated.spring(
        this.state.loginAnime,
        {toValue: primaryTheme.$deviceWidth }
      ).start();
      Animated.spring(
        this.state.signinAnime,
        {toValue: 0}
      ).start();
    }else{
      Animated.spring(
        this.state.loginAnime,
        {toValue: 0 }
      ).start();
      Animated.spring(
        this.state.signinAnime,
        {toValue: -primaryTheme.$deviceWidth}
      ).start();
    }
  }
  
  signupAnime = (pos) =>{
    if(pos === 'start'){
      Animated.spring(
        this.state.loginAnime,
        {toValue: -primaryTheme.$deviceWidth }
      ).start();
      Animated.spring(
        this.state.signupAnime,
        {toValue: 0}
      ).start();
    }else{
      Animated.spring(
        this.state.loginAnime,
        {toValue: 0 }
      ).start();
      Animated.spring(
        this.state.signupAnime,
        {toValue: -primaryTheme.$deviceWidth}
      ).start();
    }
  }
    render(){ 
      const { shift } = this.state;
      const { navigate } = this.props.navigation;
      return(
          <View style={styles.container}> 
          {this.state.isModalVisible === true ? <ModalApp data={this.state.data} changeModalView={this.changeModalView} isVisible={this.state.isModalVisible}/> : null} 
              {/* <ImageBackground  resizeMode={'stretch'} source={primaryTheme.$loginBackground} style={styles.mainBackground}> */}
              <LinearGradient  colors={['#e7effa','#bbd6fa']} style={styles.mainBackground}>
                <View style={[styles.main]}> 
                  <Animated.View style={[styles.mainView,{justifyContent:'flex-end',bottom: this.state.logoAnime}]}> 
                    <Icons.Entypo name={'shop'} style={{fontSize:400,position:'absolute',opacity:0.3,color:'white',top:-40,left:-250}}/>
                    <ImageBackground style={{width:222, height:92,zIndex:0}} source={primaryTheme.$logo}/>  
                    <View style={styles.cartsAnimeView}>
                      <CartsAnime/> 
                    </View>
                  </Animated.View>
                  <Animated.View style={[styles.mainView,{justifyContent:'flex-end',opacity: this.state.textOpacity}]}>
                    <View style={{position:'absolute',paddingLeft:30,bottom:38,transform: [{ rotate: '10deg'}]}}> 
                      <Flag code="IL" size={24}/>
                    </View>
                    <Text style={{color:'white',textAlign:'center',fontSize:14,color:'black'}}>{this.state.data.mainText}</Text>
                    <View style={{flexDirection:'row',alignItems:'center'}}>
                    <Text>{' שלנו.'}</Text><TouchableOpacity><Text style={{fontWeight:'bold',textDecorationLine:'underline'}}>{'תנאי השימוש'}</Text></TouchableOpacity><Text>{'התחברות הינך מסכים עם '}</Text>
                    </View>
                  </Animated.View>
                  <View style={[styles.mainView,{justifyContent:'flex-start'}]}>
                    <Animated.View style={[styles.mainView,{justifyContent:'flex-start',right: this.state.loginAnime}]}> 
                      <ButtonApp icon={<Icons.Foundation name={'clipboard-pencil'} style={{fontSize:24,color:'black',marginRight:5}}/>} name={this.state.data.btnTopTitle} theme={'passive'} onPress={() => this.btnClick(this.state.data.btnTopTitle, navigate)}/>
                      <ButtonApp icon={<Icons.MaterialCommunityIcons name={'login'} style={{fontSize:24,color:'black',marginRight:5}}/>} name={this.state.data.btnBottomTitle} theme={'passive'} onPress={() => this.btnClick(this.state.data.btnBottomTitle, navigate)}/>
                      <View style={{width:primaryTheme.$deviceWidth - 70,flexDirection:'row',alignItems:'center',justifyContent:'center'}}>  
                            <Counter time={1000}/>
                            <Text style={[styles.linkText,{textDecorationLine: 'none',marginRight:2}]}>{'המשלוח הבא יוצא בעוד '}</Text>
                            <Icons.FontAwesome name={'truck'} style={{fontSize:14}}/>
                        </View>
                    </Animated.View>
                    <Animated.View style={[styles.mainView,{position:'absolute', justifyContent:'flex-start',right: this.state.signinAnime }]}> 
                      <Animated.View style={[ { transform: [{translateY: shift}],alignItems:'center' }]}>    
                        <InputApp icon={<Icons.MaterialIcons name={'email'} style={{fontSize:24,color:'black',marginLeft:5}}/>} placeholder={this.state.data.inputTopTitle} theme={'passive'} onChangeText={this.onChangeTextInput} onPress={() => this.setState({inputPress: this.state.data.inputTopTitle})}/>
                        <InputApp icon={<Icons.Feather name={'lock'} style={{fontSize:24,color:'black',marginLeft:5}}/>} placeholder={this.state.data.inputBottomTitle} theme={'passive'} onChangeText={this.onChangeTextInput} onPress={() => this.setState({inputPress: this.state.data.inputBottomTitle})}/>
                        <View style={{width:primaryTheme.$deviceWidth - 70,flexDirection:'row'}}>
                          <TouchableOpacity activeOpacity={0.8} onPress={() => this.signinAnime('end')}>
                            <Text style={[styles.linkText,{textDecorationLine: 'none'}]}>{this.state.data.linkTextBack}</Text>
                          </TouchableOpacity> 
                          <TouchableOpacity style={{marginLeft:primaryTheme.$deviceWidth - 180}} activeOpacity={0.8} onPress={() => this.setState({isModalVisible: true})}>
                            <Text style={[styles.linkText,{fontWeight:'bold'}]}>{this.state.data.linkTextForgot}</Text>
                          </TouchableOpacity>
                        </View>
                      </Animated.View>
                    </Animated.View>
                    <Animated.View style={[styles.mainView,{position:'absolute', justifyContent:'flex-start',left: this.state.signupAnime }]}> 
                      <Animated.View style={[ { transform: [{translateY: shift}],alignItems:'center' }]}> 
                        <ButtonApp icon={<Icons.Entypo name={'shop'} style={{fontSize:24,color:'black',marginRight:5}}/>} name={'מכירה'} theme={'passive'} onPress={() => console.log('sell')}/>
                        <ButtonApp icon={<Icons.MaterialIcons name={'shopping-cart'} style={{fontSize:24,color:'black',marginRight:5}}/>} name={'קניה'} theme={'passive'} onPress={() => console.log('buy')}/>
                        <View style={{width:primaryTheme.$deviceWidth - 70,flexDirection:'row'}}>
                          <TouchableOpacity activeOpacity={0.8} onPress={() => this.signupAnime('end')}>
                            <Text style={[styles.linkText,{textDecorationLine: 'none'}]}>{this.state.data.linkTextBack}</Text>
                          </TouchableOpacity>  
                        </View>
                      </Animated.View>
                    </Animated.View>
                  </View>
                </View>
                </LinearGradient>
            {/* </ImageBackground> */}
          </View>
      );
    } 

    handleKeyboardDidShow = (event) => {
        const { height: windowHeight } = Dimensions.get('window');
        const keyboardHeight = event.endCoordinates.height;
        const currentlyFocusedField = TextInputState.currentlyFocusedField();
        UIManager.measure(currentlyFocusedField, (originX, originY, width, height, pageX, pageY) => {
          const fieldHeight = height;
          const fieldTop = pageY;
          let temp = this.state.inputPress === this.state.data.inputTopTitle ? 20 : -50;
          const gap = (windowHeight - keyboardHeight) - (fieldTop + fieldHeight + temp);
          if (gap >= 0) {
            return;
          }
          Animated.timing(
            this.state.logoAnime,
            {
              toValue: 50,
              duration: 400, 
            }
          ).start();
          Animated.timing(
            this.state.textOpacity,
            {
              toValue: 0,
              duration: 400, 
            }
          ).start();
          Animated.timing(
            this.state.shift,
            {
              toValue: gap - 80,
              duration: 500,
              useNativeDriver: true,
            }
          ).start();
        });
      }
      
      handleKeyboardDidHide = () => {
        Animated.timing(
          this.state.logoAnime,
          {
            toValue: 0,
            duration: 400, 
          }
        ).start();
        Animated.timing(
          this.state.textOpacity,
          {
            toValue: 1,
            duration: 900, 
          }
        ).start();
        Animated.timing(
          this.state.shift,
          {
            toValue: 0,
            duration: 500,
            useNativeDriver: true,
          }
        ).start();
      } 
}

  

  const styles = StyleSheet.create({
    container: {
      flex: 1, 
      backgroundColor:'black',
      flexDirection: 'column',
      alignItems: 'center',
      justifyContent:'center',
    }, 
    mainBackground:{
      width:'100%',
      height:'100%',
      backgroundColor:'black',
      flexDirection:'column',
      alignItems:'center',
      justifyContent:'center',
    },
    main:{
      position:'absolute',
      width:'100%',
      height: primaryTheme.$deviceHeight - primaryTheme.$deviceStatusBar,
      top:primaryTheme.$deviceStatusBar,
      flexDirection:'column',
      alignItems:'center',
      justifyContent:'center',
    },
    mainView:{
      flex:1,
      flexDirection:'column',
      alignItems:'center',
      justifyContent:'center',
    },
    linkText:{
      color: 'black',
      textDecorationLine: 'underline',
      fontSize: 14, 
    },
    cartsAnimeView:{
      width:208,
      height:100, 
      position:'absolute',
      bottom:-52, 
      left:-120,
      borderRadius:20,
      opacity:0.1, 
      alignItems:'center',
      justifyContent:'center',
      overflow:'hidden',
    }
  });
    

  const mapStateToProps = (state) => {
    return {
      initialState: state.initialState,
    };
  }; 
  export default connect(mapStateToProps)(Login);