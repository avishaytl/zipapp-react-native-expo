import React from 'react';
import { Animated, Keyboard, UIManager, Dimensions, View, StyleSheet, TextInput, Text,FlatList, ImageBackground, TouchableOpacity} from 'react-native';
import { connect } from 'react-redux'; 
import { Tabs, Tab } from 'native-base';
import HeaderApp from '../components/HeaderApp';
import FooterApp from '../components/FooterApp';
import ModalApp from '../components/ModalApp';
import MidMainView from '../components/MidMainView';
import primaryTheme from '../styles/styles'; 
import * as Icons from '@expo/vector-icons'; 
const { State: TextInputState } = TextInput;
class Main extends React.Component{
  constructor(props){
    super(props);
    this.state = { 
      shift: new Animated.Value(0), 
      isModalVisible: false,
      items:[
        {
          key:'1',
          animeHeight: new Animated.Value(148),
          animeHeightBtnTop: new Animated.Value(148/2),
          animeHeightBtnBottom: new Animated.Value(148/2),
          animeBtnTextOpacity: new Animated.Value(1),
          animeImageLinkOpacity: new Animated.Value(0), 
          symbol: 'AUD/USD',
          category: 'Forex',
          position: 'Buy Now',
          entry: '0.67432',
          sl: '0.67432',
          date: '10/02/19 15:56',
          user: 'Robert',
          labelTop: 'LABEL',
          labelTopColor: '#6d7278',
          labelBottom: 'EXECUTE\nTRADE\n(29:16)',
          labelBottomColor: '#fa6400',
        },
        {
          key:'2',
          animeHeight: new Animated.Value(148),
          animeHeightBtnTop: new Animated.Value(148/2),
          animeHeightBtnBottom: new Animated.Value(148/2),
          animeBtnTextOpacity: new Animated.Value(1),
          animeImageLinkOpacity: new Animated.Value(0), 
          symbol: 'AUD/USD',
          category: 'Forexs',
          position: 'Buy Now',
          entry: '0.67432',
          sl: '0.67432',
          date: '10/02/19 15:56',
          user: 'Robert',
          labelTop: 'LABEL',
          labelTopColor: '#4d9500',
          labelBottom: 'EXECUTE\nTRADE\n(29:16)',
          labelBottomColor: '#fa6400',
        },
        {
          key:'3',
          animeHeight: new Animated.Value(148),
          animeHeightBtnTop: new Animated.Value(148/2),
          animeHeightBtnBottom: new Animated.Value(148/2),
          animeBtnTextOpacity: new Animated.Value(1),
          animeImageLinkOpacity: new Animated.Value(0), 
          symbol: 'AUD/USD',
          category: 'Forex',
          position: 'Buy Now',
          entry: '0.67432',
          sl: '0.67432',
          date: '10/02/19 15:56',
          user: 'Robert',
          labelTop: 'LABEL',
          labelTopColor: '#fe2501',
          labelBottom: 'EXECUTE\nTRADE\n(29:16)',
          labelBottomColor: '#fa6400',
        },
        {
          key:'4',
          animeHeight: new Animated.Value(148),
          animeHeightBtnTop: new Animated.Value(148/2),
          animeHeightBtnBottom: new Animated.Value(148/2),
          animeBtnTextOpacity: new Animated.Value(1),
          animeImageLinkOpacity: new Animated.Value(0), 
          symbol: 'AUD/USD',
          category: 'Forex',
          position: 'Buy Now',
          entry: '0.67432',
          sl: '0.67432',
          date: '10/02/19 15:56',
          user: 'Robert',
          labelTop: 'LABEL',
          labelTopColor: '#0091ff',
          labelBottom: 'EXECUTE\nTRADE\n(29:16)',
          labelBottomColor: '#fa6400',
        },
        {
          key:'5',
          animeHeight: new Animated.Value(148),
          animeHeightBtnTop: new Animated.Value(148/2),
          animeHeightBtnBottom: new Animated.Value(148/2),
          animeBtnTextOpacity: new Animated.Value(1),
          animeImageLinkOpacity: new Animated.Value(0), 
          symbol: 'AUD/USD',
          category: 'Forex',
          position: 'Buy Now',
          entry: '0.67432',
          sl: '0.67432',
          date: '10/02/19 15:56',
          user: 'Robert',
          labelTop: 'LABEL',
          labelTopColor: '#4d9500',
          labelBottom: 'EXECUTE\nTRADE\n(29:16)',
          labelBottomColor: '#fa6400',
        },
        {
          key:'6',
          animeHeight: new Animated.Value(148),
          animeHeightBtnTop: new Animated.Value(148/2),
          animeHeightBtnBottom: new Animated.Value(148/2),
          animeBtnTextOpacity: new Animated.Value(1),
          animeImageLinkOpacity: new Animated.Value(0), 
          symbol: 'AUD/USD',
          category: 'Forex',
          position: 'Buy Now',
          entry: '0.67432',
          sl: '0.67432',
          date: '10/02/19 15:56',
          user: 'Robert',
          labelTop: 'LABEL',
          labelTopColor: '#fe2501',
          labelBottom: 'EXECUTE\nTRADE\n(29:16)',
          labelBottomColor: '#fa6400',
        },
        {
          key:'7',
          animeHeight: new Animated.Value(148),
          animeHeightBtnTop: new Animated.Value(148/2),
          animeHeightBtnBottom: new Animated.Value(148/2),
          animeBtnTextOpacity: new Animated.Value(1),
          animeImageLinkOpacity: new Animated.Value(0), 
          symbol: 'AUD/USD',
          category: 'Forex',
          position: 'Buy Now',
          entry: '0.67432',
          sl: '0.67432',
          date: '10/02/19 15:56',
          user: 'Robert',
          labelTop: 'LABEL',
          labelTopColor: '#0091ff',
          labelBottom: 'EXECUTE\nTRADE\n(29:16)',
          labelBottomColor: '#fa6400',
        },
        {
          key:'8',
          animeHeight: new Animated.Value(148),
          animeHeightBtnTop: new Animated.Value(148/2),
          animeHeightBtnBottom: new Animated.Value(148/2),
          animeBtnTextOpacity: new Animated.Value(1),
          animeImageLinkOpacity: new Animated.Value(0), 
          symbol: 'AUD/USD',
          category: 'Forex',
          position: 'Buy Now',
          entry: '0.67432',
          sl: '0.67432',
          date: '10/02/19 15:56',
          user: 'Robert',
          labelTop: 'LABEL',
          labelTopColor: '#6d7278',
          labelBottom: 'EXECUTE\nTRADE\n(29:16)',
          labelBottomColor: '#fa6400',
        }  
      ],
      data:{
        itemText1: 'Category',
        itemText2: 'Position',
        itemText3: 'Entry',
        itemText4: 'SL',
        itemLinkText: 'Show More',
      }
    } 
  } 
  changeModalView = (val) =>{ 
    this.setState({isModalVisible: val});
  }
  UNSAFE_componentWillMount() {
    this.keyboardDidShowSub = Keyboard.addListener('keyboardDidShow', this.handleKeyboardDidShow);
    this.keyboardDidHideSub = Keyboard.addListener('keyboardDidHide', this.handleKeyboardDidHide);
  }

  renderItem =({ item }) => {
    return(
      <Animated.View style={[styles.item,{height: item.animeHeight}]}>
        <View style={{height:148,flexDirection:'row'}}>
            <View style={[styles.itemInside,{alignItems:'flex-start',paddingTop:10,paddingLeft:10}]}>
              <Text style={styles.itemTitle}>{item.symbol}</Text>
              <Text style={styles.itemText}>{this.state.data.itemText1}</Text>
              <Text style={styles.itemText}>{this.state.data.itemText2}</Text>
              <Text style={styles.itemText}>{this.state.data.itemText3}</Text>
              <Text style={styles.itemText}>{this.state.data.itemText4}</Text>
              <Text style={styles.itemText}>{item.date}</Text>
            </View>
            <View style={[styles.itemInside,{alignItems:'flex-end',paddingTop:10,paddingRight:10}]}>
              <Text style={[styles.itemTitle,{opacity:0}]}>Title</Text>
              <Text style={[styles.itemText,{fontWeight:'bold',color:'black'}]}>{item.category}</Text>
              <Text style={[styles.itemText,{fontWeight:'bold',color:'black'}]}>{item.position}</Text>
              <Text style={[styles.itemText,{fontWeight:'bold',color:'black'}]}>{item.entry}</Text>
              <Text style={[styles.itemText,{fontWeight:'bold',color:'black'}]}>{item.sl}</Text>
              <Text style={styles.itemText}>{item.user}</Text>
            </View>
            <View style={[styles.itemInside]}>
              <Animated.View style={[styles.itemBtnView,{height: item.animeHeightBtnTop}]}>
              {/* this.changeItemHeight(item.animeHeight, item.animeHeightBtnBottom, item.animeHeightBtnTop, item.animeBtnTextOpacity, item.animeImageLinkOpacity) */}
                <TouchableOpacity activeOpacity={0.8} onPress={()=> this.props.navigation.navigate('MainTab')}  style={[styles.itemBtn,{backgroundColor: item.labelTopColor}]}>
                  <Text style={{textAlign:'center',color:'white'}}>{item.labelTop}</Text>
                </TouchableOpacity>
              </Animated.View>
              <Animated.View style={[styles.itemBtnView,{height: item.animeHeightBtnBottom}]}>
                <TouchableOpacity activeOpacity={0.8} style={[styles.itemBtn,{backgroundColor: item.labelBottomColor}]}>
                  <Animated.Text style={{textAlign:'center',color:'white',opacity: item.animeBtnTextOpacity}}>{item.labelBottom}</Animated.Text>
                </TouchableOpacity>
              </Animated.View>
            </View>
          </View>
          <Animated.View style={{opacity: item.animeImageLinkOpacity,height:407-148,flexDirection:'column',alignItems:'center',justifyContent:'center'}}>
            <View style={{backgroundColor:'#f3f3f3',height:150,width:'90%',borderRadius: 10,alignItems:'center',justifyContent:'center'}}>
                <Text style={styles.itemTitle}>IMAGE</Text>
            </View>
            <TouchableOpacity activeOpacity={0.8} onPress={() => this.setState({isModalVisible: true})}>
              <Text style={[styles.itemTitle,{marginTop:25,color:'#42c7de',textDecorationLine:'underline'}]}>{this.state.data.itemLinkText}</Text>
            </TouchableOpacity>
          </Animated.View>
      </Animated.View>
    );
  }
  componentWillUnmount() {
    this.keyboardDidShowSub.remove();
    this.keyboardDidHideSub.remove();
  }
    render(){
      const { shift } = this.state;
      const { navigate } = this.props.navigation;
      return(
          <View style={styles.container}> 
          {this.state.isModalVisible === true ? <ModalApp data={this.state.data} changeModalView={this.changeModalView} isVisible={this.state.isModalVisible}/> : null} 
              <ImageBackground  resizeMode={'stretch'}  style={styles.mainBackground}>
                <HeaderApp navigate={navigate}/>
                {/* <HeaderSlider/> */}
                <View style={styles.main}>
                    <View style={styles.mainView}>
                      <View style={styles.labelView}>
                        <Icons.Ionicons style={{paddingTop:2}} name={'ios-arrow-back'}/>
                        <Icons.Ionicons style={{paddingTop:2,paddingRight:4}} name={'ios-arrow-back'}/>
                        <Text>חנויות</Text>
                      </View> 
                    </View>
                    <View style={[styles.mainView,{paddingRight:0,alignItems:'center',justifyContent:'center',borderColor:'black',borderWidth:0.5,borderRadius:1,borderStyle:'dashed'}]}>  
                      <MidMainView />  
                    </View>
                    <View style={styles.mainView}>
                      <View style={styles.labelView}>
                        <Icons.Ionicons style={{paddingTop:2}} name={'ios-arrow-back'}/>
                        <Icons.Ionicons style={{paddingTop:2,paddingRight:4}} name={'ios-arrow-back'}/>
                        <Text>מוצרים</Text> 
                      </View> 
                    </View>
                </View>
                {/* <FlatList style={styles.main}
                  data={this.state.items} 
                  contentContainerStyle={{flexGrow: 1, justifyContent: 'center',alignItems:'center',padding:10}}
                  renderItem={this.renderItem} 
                  scrollEnabled={true}
                />  */}
                {/* <FooterApp /> */} 
              </ImageBackground>
          </View>
      );
    }
  
  changeItemHeight = (height, btnHeightBottom, btnHeightTop, btnTextOpacity, ImageLinkOpacity) =>{ 
    if(height._value < 407){
      Animated.timing(
        height,
        {
          toValue: 407,
          duration: 400, 
        }
      ).start();
      Animated.timing(
        btnHeightBottom,
        {
          toValue: 0,
          duration: 400, 
        }
      ).start();
      Animated.timing(
        btnHeightTop,
        {
          toValue: 148,
          duration: 400, 
        }
      ).start();
      Animated.timing(
        btnTextOpacity,
        {
          toValue: 0,
          duration: 100, 
        }
      ).start();
      Animated.timing(
        ImageLinkOpacity,
        {
          toValue: 1,
          duration: 500, 
        }
      ).start();
    }else{
      Animated.timing(
        height,
        {
          toValue: 148,
          duration: 400, 
        }
      ).start();
      Animated.timing(
        btnHeightBottom,
        {
          toValue: 148/2,
          duration: 400, 
        }
      ).start();
      Animated.timing(
        btnHeightTop,
        {
          toValue: 148/2,
          duration: 400, 
        }
      ).start();
      Animated.timing(
        btnTextOpacity,
        {
          toValue: 1,
          duration: 600, 
        }
      ).start();
      Animated.timing(
        ImageLinkOpacity,
        {
          toValue: 0,
          duration: 300, 
        }
      ).start();
    }
  }
    handleKeyboardDidShow = (event) => {
      const { height: windowHeight } = Dimensions.get('window');
      const keyboardHeight = event.endCoordinates.height;
      const currentlyFocusedField = TextInputState.currentlyFocusedField();
      UIManager.measure(currentlyFocusedField, (originX, originY, width, height, pageX, pageY) => {
        const fieldHeight = height;
        const fieldTop = pageY;
        const gap = (windowHeight - keyboardHeight) - (fieldTop + fieldHeight);
        if (gap >= 0) {
          return;
        }
        Animated.timing(
          this.state.shift,
          {
            toValue: gap - 100,
            duration: 500,
            useNativeDriver: true,
          }
        ).start();
      });
    }
    
    handleKeyboardDidHide = () => {
      Animated.timing(
        this.state.shift,
        {
          toValue: 0,
          duration: 500,
          useNativeDriver: true,
        }
      ).start();
    } 
}

  

  const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor:'black',
      flexDirection: 'column',
      alignItems: 'center',
      justifyContent:'center',
    }, 
    tabStyle:{
      backgroundColor: 'blue',
    },
    main:{
      width:'100%',
      height: primaryTheme.$deviceHeight - primaryTheme.$deviceStatusBar - 100 ,
      position:'absolute',
      bottom:0, 
    },
    mainView:{
      flex:1,
      flexDirection:'row',
      alignItems:'flex-start',
      justifyContent:'flex-end',
      paddingRight:10,
    },
    labelView:{ 
      flexDirection:'row',
      alignItems:'center',
      justifyContent:'center',
    },
    mainBackground:{
      width:'100%',
      height:'100%',
      backgroundColor:'white',
      flexDirection:'column',
      alignItems:'center',
      justifyContent:'center',
    },
    itemText:{
      fontSize:16,
      color:'#6d7278',
    },
    itemTitle:{
      fontSize:18,
      fontWeight:'bold',
  },
    item:{ 
      width: primaryTheme.$deviceWidth - 50,
      backgroundColor:'#e5fbfd',
      borderRadius:10,
      margin:10,
      shadowColor: "#000",
      shadowOffset: {
          width: 0,
          height: 2,
      },
      shadowOpacity: 0.20,
      shadowRadius: 5, 
      elevation: 20,
      flexDirection:'column', 
    },
    itemInside:{
      flex:1,
      maxHeight: 148
    },
    itemBtnView:{
      height:148/2,
      alignItems:'center',
      justifyContent:'center'
    },
    itemBtn:{
      width:'90%',
      height:'90%',
      borderRadius:10,
      backgroundColor:'white',
      alignItems:'center',
      justifyContent:'center'
    }
  });
    

  const mapStateToProps = (state) => {
    return {
      initialState: state.initialState,
    };
  };
  // Exports
  export default connect(mapStateToProps)(Main);