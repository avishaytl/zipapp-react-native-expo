import React from 'react';
import { Animated, Keyboard, UIManager, Dimensions, View, StyleSheet, TextInput, Text, ImageBackground, Image} from 'react-native';
import { connect } from 'react-redux'; 
import primaryTheme from '../styles/styles';
import FooterTabs from '../components/FooterTabs';
import { LinearGradient } from 'expo-linear-gradient';
const { State: TextInputState } = TextInput; 
class Signup extends React.Component{
  constructor(props){
    super(props);
    this.state = { 
      shift: new Animated.Value(0), 
      data:{
        mainTitle: 'ברוך הבא!', 
      }
    }  
  } 
  UNSAFE_componentWillMount() {
    this.keyboardDidShowSub = Keyboard.addListener('keyboardDidShow', this.handleKeyboardDidShow);
    this.keyboardDidHideSub = Keyboard.addListener('keyboardDidHide', this.handleKeyboardDidHide);
  }

  componentWillUnmount() {
    this.keyboardDidShowSub.remove();
    this.keyboardDidHideSub.remove();
  } 
    render(){
      const { shift } = this.state;
      const { navigate } = this.props.navigation;
      return(
          <View style={styles.container}> 
            <LinearGradient  colors={['#e7effa','#bbd6fa']} style={styles.mainBackground}>
              <ImageBackground style={styles.mainBackground}> 
                    <View style={styles.header}> 
                      <Text style={styles.mainTitle}>{this.state.data.mainTitle}</Text> 
                    </View>
                    {/* <FooterTabs navigate={navigate} changeSmallTitle={this.changeSmallTitle}/>  */}
              </ImageBackground>
            </LinearGradient>
          </View>
      );
    }
  
    handleKeyboardDidShow = (event) => {
      const { height: windowHeight } = Dimensions.get('window');
      const keyboardHeight = event.endCoordinates.height;
      const currentlyFocusedField = TextInputState.currentlyFocusedField();
      UIManager.measure(currentlyFocusedField, (originX, originY, width, height, pageX, pageY) => {
        const fieldHeight = height;
        const fieldTop = pageY;
        const gap = (windowHeight - keyboardHeight) - (fieldTop + fieldHeight);
        if (gap >= 0) {
          return;
        }
        Animated.timing(
          this.state.shift,
          {
            toValue: gap - 100,
            duration: 500,
            useNativeDriver: true,
          }
        ).start();
      });
    }
    
    handleKeyboardDidHide = () => {
      Animated.timing(
        this.state.shift,
        {
          toValue: 0,
          duration: 500,
          useNativeDriver: true,
        }
      ).start();
    } 
}

  

  const styles = StyleSheet.create({
    container: {
      flex: 1,  
      flexDirection: 'column',
      alignItems: 'center',
      justifyContent:'center',
    }, 
    mainBackground:{
      width:'100%', 
      height: primaryTheme.$deviceHeight, 
      // flexDirection:'column',
      // alignItems:'center',
      // justifyContent:'center',
    }, 
    header:{ 
      top: primaryTheme.$deviceStatusBar,
      padding:20,
      width:'100%', 
      flexDirection: 'column', 
      justifyContent:'flex-start',
      alignItems: 'flex-end', 
    },
    mainTitle:{
      fontSize: 28,
      fontWeight: 'bold'
    }, 
  });
    

  const mapStateToProps = (state) => {
    return {
      initialState: state.initialState,
    };
  };
  // Exports
  export default connect(mapStateToProps)(Signup);