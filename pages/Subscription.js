import React from 'react';
import { Animated, Keyboard, UIManager, Dimensions, View, StyleSheet, TouchableOpacity, TextInput, Text} from 'react-native';
import { connect } from 'react-redux'; 
const { State: TextInputState } = TextInput;
const getAvailableRoutes = navigation => {
  let availableRoutes = [];
  if (!navigation) return availableRoutes;

  const parent = navigation.dangerouslyGetParent();
  if (parent) {
    if (parent.router && parent.router.childRouters) {
      // Grab all the routes the parent defines and add it the list
      availableRoutes = [
        ...availableRoutes,
        ...Object.keys(parent.router.childRouters),
      ];
    }

    // Recursively work up the tree until there are none left
    availableRoutes = [...availableRoutes, ...getAvailableRoutes(parent)];
  }

  // De-dupe the list and then remove the current route from the list
  return [...new Set(availableRoutes)].filter(
    route => route !== navigation.state.routeName
  );
};

class Subscription extends React.Component{
  constructor(props){
    super(props);
    this.state = { 
      shift: new Animated.Value(0), 
    } 
  } 
  UNSAFE_componentWillMount() {
    this.keyboardDidShowSub = Keyboard.addListener('keyboardDidShow', this.handleKeyboardDidShow);
    this.keyboardDidHideSub = Keyboard.addListener('keyboardDidHide', this.handleKeyboardDidHide);
  }

  componentWillUnmount() {
    this.keyboardDidShowSub.remove();
    this.keyboardDidHideSub.remove();
  }
    render(){
      const { shift } = this.state;
      const { navigate } = this.props.navigation;
      return(
          <View style={styles.container}>    
          {/* {getAvailableRoutes(this.props.navigation).map(route => (
            <TouchableOpacity
              onPress={() => this.props.navigation.navigate(route)}
              key={route}
              style={{
                backgroundColor: '#fff',
                padding: 10,
                margin: 10,
              }}
            >
              <Text>{route}</Text>
            </TouchableOpacity>
          ))} */}
          <TouchableOpacity onPress={() => this.props.navigation.navigate('Signin')}>
              <Text style={{color:'white',}}>Subscription-screen</Text> 
              </TouchableOpacity>
          </View>
      );
    }
  
    handleKeyboardDidShow = (event) => {
      const { height: windowHeight } = Dimensions.get('window');
      const keyboardHeight = event.endCoordinates.height;
      const currentlyFocusedField = TextInputState.currentlyFocusedField();
      UIManager.measure(currentlyFocusedField, (originX, originY, width, height, pageX, pageY) => {
        const fieldHeight = height;
        const fieldTop = pageY;
        const gap = (windowHeight - keyboardHeight) - (fieldTop + fieldHeight);
        if (gap >= 0) {
          return;
        }
        Animated.timing(
          this.state.shift,
          {
            toValue: gap - 100,
            duration: 500,
            useNativeDriver: true,
          }
        ).start();
      });
    }
    
    handleKeyboardDidHide = () => {
      Animated.timing(
        this.state.shift,
        {
          toValue: 0,
          duration: 500,
          useNativeDriver: true,
        }
      ).start();
    } 
}

  

  const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor:'gray',
      flexDirection: 'column',
      alignItems: 'center',
      justifyContent:'center',
    }, 
  });
    

  const mapStateToProps = (state) => {
    return {
      initialState: state.initialState,
    };
  };
  // Exports
  export default connect(mapStateToProps)(Subscription);