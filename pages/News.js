import React from 'react';
import { Animated, Keyboard, UIManager, Dimensions, View, StyleSheet, TextInput, Text} from 'react-native';
import { connect } from 'react-redux'; 
const { State: TextInputState } = TextInput;
class News extends React.Component{
  constructor(props){
    super(props);
    this.state = { 
      shift: new Animated.Value(0), 
    } 
  } 
  UNSAFE_componentWillMount() {
    this.keyboardDidShowSub = Keyboard.addListener('keyboardDidShow', this.handleKeyboardDidShow);
    this.keyboardDidHideSub = Keyboard.addListener('keyboardDidHide', this.handleKeyboardDidHide);
  }

  componentWillUnmount() {
    this.keyboardDidShowSub.remove();
    this.keyboardDidHideSub.remove();
  }
    render(){
      const { shift } = this.state;
      const { navigate } = this.props.navigation;
      return(
          <View style={styles.container}> 
              <Text style={{color:'white',}}>News-screen</Text>
          </View>
      );
    }
  
    handleKeyboardDidShow = (event) => {
      const { height: windowHeight } = Dimensions.get('window');
      const keyboardHeight = event.endCoordinates.height;
      const currentlyFocusedField = TextInputState.currentlyFocusedField();
      UIManager.measure(currentlyFocusedField, (originX, originY, width, height, pageX, pageY) => {
        const fieldHeight = height;
        const fieldTop = pageY;
        const gap = (windowHeight - keyboardHeight) - (fieldTop + fieldHeight);
        if (gap >= 0) {
          return;
        }
        Animated.timing(
          this.state.shift,
          {
            toValue: gap - 100,
            duration: 500,
            useNativeDriver: true,
          }
        ).start();
      });
    }
    
    handleKeyboardDidHide = () => {
      Animated.timing(
        this.state.shift,
        {
          toValue: 0,
          duration: 500,
          useNativeDriver: true,
        }
      ).start();
    } 
}

  

  const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor:'gray',
      flexDirection: 'column',
      alignItems: 'center',
      justifyContent:'center',
    }, 
  });
    

  const mapStateToProps = (state) => {
    return {
      initialState: state.initialState,
    };
  };
  // Exports
  export default connect(mapStateToProps)(News);