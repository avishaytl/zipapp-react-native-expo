export const primaryValues = {
    $GET_LOGIN: 'GET_LOGIN',
    $LOGIN_RESULT: 'LOGIN_RESULT',
    $LOGIN_ERROR: 'LOGIN_ERROR', 
}
export default primaryValues;