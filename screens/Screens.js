import { createStackNavigator } from 'react-navigation-stack';
import { createAppContainer } from 'react-navigation';
import { createBottomTabNavigator } from 'react-navigation-tabs';
import { fromLeft } from 'react-navigation-transitions';
import LoginScreen from '../pages/Login'; 
import OnboardingScreen from '../pages/Onboarding'; 
import MainScreen from '../pages/Main';
import MainTab from '../pages/MainTab';  
import StatsScreen from '../pages/Stats'; 
import NewsScreen from '../pages/News'; 
import SettingsScreen from '../pages/Settings'; 
import MetaScreen from '../pages/Meta'; 
import SubscriptionScreen from '../pages/Subscription';
import SigninScreen from '../pages/Signin'; 
import SignupScreen from '../pages/Signup';  
import React from 'react';
import { View, StyleSheet, Text, TouchableOpacity, Modal, TextInput} from 'react-native'; 
import * as Icons from '@expo/vector-icons';

const MainTabStack = createStackNavigator({
  MainScreen: {
    screen: MainScreen,
    navigationOptions: {
        header: null,
      },
  },
  MainTab: {
    screen: MainTab,
    navigationOptions: {
        header: null,
      },
  }, 
},    
{ 
  transitionConfig: () => fromLeft(),
});

const NewsTabStack = createStackNavigator({
  NewsScreen: {
    screen: NewsScreen,
    navigationOptions: {
        header: null,
      },
  },
  // Signin: {
  //   screen: SigninScreen,
  //   navigationOptions: {
  //       header: null,
  //     },
  // }, 
},    
{ 
  transitionConfig: () => fromLeft(),
});

const MainTabsStack = createBottomTabNavigator({
  Main: {
    screen: MainTabStack,
    navigationOptions: ()=>({ 
      tabBarLabel: ' ',
      tabBarIcon:({tintColor })=> 
        <View style={{ position:'absolute',  width:'100%', flexDirection: 'column', alignItems:'center', justifyContent:'center',}}>
            <Icons.MaterialCommunityIcons style={{ color: tintColor,fontSize:24}} name={'newspaper'}/>
            <Text style={{color: tintColor,fontSize:16, margin:0}}>{'ראשי'}</Text>
        </View> ,
      tabBarOptions:{   
        tabStyle:{ 
          flexDirection: 'row',
          alignItems:'center',
          justifyContent:'center',   
        },
        activeTintColor: 'black', // active icon color
        inactiveTintColor: 'gray',  // inactive icon color
        style: {
            backgroundColor: 'white', // TabBar background, 
        },
        labelStyle:{
          fontSize: 16,   
        },
      }
    }),
  },
  Trades: {
    screen: StatsScreen,
    navigationOptions: ()=>({
      tabBarLabel: ' ',
      tabBarIcon:({tintColor})=> 
        <View style={{ position:'absolute',  width:'100%', flexDirection: 'column', alignItems:'center', justifyContent:'center',}}>
            <Icons.Entypo style={{ color: tintColor,fontSize:24}} name={'shop'}/>
            <Text style={{color: tintColor,fontSize:16, margin:0}}>{'חנויות'}</Text>
        </View> ,
      tabBarOptions:{   
        tabStyle:{ 
          flexDirection: 'row',
          alignItems:'center',
          justifyContent:'center',   
        },
        activeTintColor: 'black', // active icon color
        inactiveTintColor: 'gray',  // inactive icon color
        style: {
            backgroundColor: 'white', // TabBar background, 
        },
        labelStyle:{
          fontSize: 16,   
        },
      }
    }),
  },
  News: {
    screen: NewsTabStack,
    navigationOptions: ()=>({
      tabBarLabel: ' ',
      tabBarIcon:({tintColor})=> 
        <View style={{ position:'absolute',  width:'100%', flexDirection: 'column', alignItems:'center', justifyContent:'center',}}>
            <Icons.MaterialIcons style={{ color: tintColor,fontSize:24}} name={'shopping-cart'}/>
            <Text style={{color: tintColor,fontSize:16 ,margin:0}}>{'עגלה'}</Text>
        </View> ,
      tabBarOptions:{   
        tabStyle:{ 
          flexDirection: 'row',
          alignItems:'center',
          justifyContent:'center',   
        },
        activeTintColor: 'black', // active icon color
        inactiveTintColor: 'gray',  // inactive icon color
        style: {
            backgroundColor: 'white', // TabBar background, 
        },
        labelStyle:{
          fontSize: 16,   
        },
      }
    }),
  },
  Settings: {
    screen: SettingsScreen,
    navigationOptions: ()=>({
      tabBarLabel: ' ',
      tabBarIcon:({tintColor})=> 
        <View style={{ position:'absolute',  width:'100%', flexDirection: 'column', alignItems:'center', justifyContent:'center',}}>
            <Icons.MaterialCommunityIcons style={{ color: tintColor,fontSize:24}} name={'clipboard-account'}/>
            <Text style={{color: tintColor,fontSize:16, margin:0}}>{'חשבון'}</Text>
        </View> ,
      tabBarOptions:{   
        tabStyle:{ 
          flexDirection: 'row',
          alignItems:'center',
          justifyContent:'center',   
        },
        activeTintColor: 'black', // active icon color
        inactiveTintColor: 'gray',  // inactive icon color
        style: {
            backgroundColor: 'white', // TabBar background, 
        },
        labelStyle:{
          fontSize: 16,   
        },
      }
    }),
  },
});
 

const CenterAppStack = createStackNavigator({
  LoginScreen: { 
        screen: LoginScreen,
        navigationOptions: {
            header: null,
          },
    }, 
  SignupScreen: { 
        screen: SignupScreen,
        navigationOptions: {
            header: null,
          },
    },
  OnboardingScreen: {   
        screen: OnboardingScreen,
        navigationOptions: {
            header: null,
          },
    }, 
  MainScreen: {    
        screen: MainTabsStack,
        navigationOptions: {
            header: null,
          },
    }, 
  MetaScreen: {   
        screen: MetaScreen,
        navigationOptions: {
            header: null,
          },
    },  
},    
{
  initialRouteName: 'LoginScreen',
  transitionConfig: () => fromLeft(),
});
  const Screens = createAppContainer(CenterAppStack);
  export default Screens;